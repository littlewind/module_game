import 'package:flutter/material.dart';
import 'package:game/src/model/game/flash_game_object.dart';
import 'package:game/src/model/game/game_object.dart';
import 'package:game/src/model/game/matching_game_object.dart';
import 'package:game/src/model/game/quiz_game_object.dart';
import 'package:game/src/model/game/spelling_game_object.dart';
import 'package:game/src/screen/study/flash_card_view.dart';
import 'package:game/src/screen/study/matching_view.dart';
import 'package:game/src/screen/study/quiz_view.dart';
import 'package:game/src/screen/study/spelling_view.dart';
import 'package:game/src/screen/study/study_screen.dart';

typedef OnAnswer<T>(AnswerType type, [T? params]);

class GameItemView extends StatelessWidget {
  final GameObject gameObject;
  final OnAnswer? onAnswer;

  GameItemView({Key? key, required this.gameObject, this.onAnswer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (gameObject is QuizGameObject) {
      return QuizView(
        onAnswer: onAnswer,
        gameObject: gameObject as QuizGameObject,
      );
    } else if (gameObject is FlashGameObject) {
      return FlashCardView(
        onAnswer: onAnswer,
        gameObject: gameObject as FlashGameObject,
      );
    } else if (gameObject is SpellingGameObject) {
      return SpellingView(
        onAnswer: onAnswer!,
        gameObject: gameObject as SpellingGameObject,
      );
    }  else if (gameObject is MatchingGameObject) {
      return MatchingView(
        onAnswer: onAnswer!,
        gameObject: gameObject as MatchingGameObject,
      );
    } else {
      return Center(
        child: Text("undefined game"),
      );
    }
  }
}
