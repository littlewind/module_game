import 'package:flutter/material.dart';
import 'package:game/src/screen/study/study_screen.dart';
import 'package:models/module_models.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  final String title = 'Home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Vocab'),
          onPressed: () {
            final topicId = '4735054791049216';
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (_) => StudyScreen(Topic(
                      id: topicId,
                      parentId: 'parentId',
                      topicContentId: 'topicContentId',
                      topicExerciseId: 'topicExerciseId',
                      userId: 'userId',
                      courseId: 'courseId',
                      name: 'Topic Name',
                      description: 'description',
                      shortDescription: 'shortDescription',
                      type: 0,
                      status: 0,
                      startTime: 0,
                      endTime: 0,
                      lastUpdate: 0,
                      slug: 'slug',
                      orderIndex: 0,
                      password: 'password',
                      childType: 0,
                      avatar: 'avatar',
                      topicExercise: null))),
            );
          },
        ),
      ),
    );
  }
}
