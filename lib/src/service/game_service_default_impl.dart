import 'dart:convert';
import 'package:game/src/utils/constant.dart';
import 'package:http/http.dart' as http;
import 'package:game/src/service/game_service.dart';
import 'package:models/module_models.dart';

class GameServiceDefaultImpl implements GameService {
  @override
  Future<List<Question>> loadQuestionsByParentId(
      {required String parentId}) async {
    final body = jsonEncode({
      "topicId": parentId,
    });
    final response = await http.post(
      Uri.parse(cardEndpoint),
      headers: defaultHeader,
      body: body,
    );
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List;
      final List<Card> cardList = json.map((e) {
        return Card.fromJson(e);
      }).toList();

      return cardList.map((card) => Question.fromCard(card)).toList();
    }
    return [];
  }

  @override
  Future<List<Question>> loadChildQuestionList(
      Map<String, Question> mapHasChild) {
    return Future.value(
      mapHasChild.values.expand((question) => question.children).toList(),
    );
  }

  @override
  navigateAfterFinishingStudy() {}
}
