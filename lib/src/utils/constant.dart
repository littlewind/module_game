import 'package:game/src/service/custom_constants.dart';

const String GOOGLE_CLOUD_STORAGE_URL = "https://storage.googleapis.com";
const CONFIG_TEST_MODE = true;

// const int TYPE_CARD_NORMAL = 0;
// const int TYPE_CARD_PARAGRAP = 1;
// const int TYPE_CARD_PARAGRAP_CHILD = -1;

// const int TOPIC_MODE_NORMAL = 0;

const Map<String, String> defaultHeader = {
  'Content-Type': 'application/json',
};

const String cardEndpoint = BASE_URL + cardPath;

class AppFont {
  static const FONT_SF_PRO = "SF Pro";
}