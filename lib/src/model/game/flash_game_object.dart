import 'package:game/src/model/game/game_object.dart';
import 'package:models/module_models.dart';

class FlashGameObject extends GameObject {
  late Face answer;
  FlashGameObject.fromQuestion(Question questionDb) : super.fromQuestion(questionDb) {
    answer = Face()..content = questionDb.choices!.first.content;
  }

  onAnswer() {
    if (status == GameObjectStatus.answered) {
      return;
    } else {
      status = GameObjectStatus.answered;
      questionStatus = QuestionStatus.answeredCorrect;
    }
  }

}