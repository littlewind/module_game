import 'package:collection/collection.dart' show IterableExtension;
import 'package:game/src/model/game/game_object.dart';
import 'package:game/src/screen/study/spelling_view.dart';
import 'package:models/module_models.dart';

class SpellingGameObject extends GameObject {
  late Face answer;
  String userInput = '';

  SpellingGameObject.fromQuestion(Question questionDb) : super.fromQuestion(questionDb) {
    String content = questionDb.content!.replaceAll(' ', '');
    answer = Face()
      ..content = content
      ..id = questionDb.id;
    if (questionDb.choices!.isNotEmpty) {
      Choice correctChoice = questionDb.choices!
          .firstWhereOrNull((element) => element.isCorrect)!;
      question = Face()
        ..content = correctChoice.content
        ..id = questionDb.id;
    }
  }

  onAnswer(SpellAnswerParams params) {
    switch (params.type) {
      case SpellCardAnswerType.send_answer:
        if (status == GameObjectStatus.answered) {
          return;
        }
        userInput = params.answer;
        if (userInput.toLowerCase() == answer.content!.toLowerCase()) {
          status = GameObjectStatus.answered;
          questionStatus = QuestionStatus.answeredCorrect;
        } else {
          status = GameObjectStatus.answered;
          questionStatus = QuestionStatus.answeredIncorrect;
        }
        break;
      case SpellCardAnswerType.user_input_value:
        userInput = params.answer;
        break;
      default:
        break;
    }
  }

  reset() {
    super.reset();
    userInput = '';
    userInput = userInput.padRight(answer.content!.length, '_');
  }

}