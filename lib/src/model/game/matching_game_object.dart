import 'package:collection/collection.dart' show IterableExtension;
import 'package:game/src/model/game/game_object.dart';
import 'package:game/src/screen/study/matching_view.dart';
import 'package:models/module_models.dart';

class MatchingGameObject extends GameObject {
  List<Question> questions = [];

  int clickNumAvailable = 0;

  MatchingGameObject.fromQuestions(List<Question> _questions): super.fromQuestion(_questions.first) {
    _questions.forEach((element) {
      if (element.content!.isEmpty || element.choices!.isEmpty) {
        return;
      }
      questions.add(Question()
        ..content = element.content
        ..id = element.id);
      if (element.choices!.isNotEmpty) {
        Choice correctChoice = element.choices!
            .firstWhereOrNull((element) => element.isCorrect)!;
        questions.add(Question()
          ..content = correctChoice.content
          ..id = element.id);
      }
    });
    questions.shuffle();
    clickNumAvailable = questions.length + 2;
  }

  onAnswer(MatchingAnswerParams params) {
    clickNumAvailable -= 2;
    final isMatch = params.question1.id == params.question2.id;
    if (isMatch) {
      params.question1.questionStatus = QuestionStatus.answeredCorrect;
      params.question2.questionStatus = QuestionStatus.answeredCorrect;
      params.onAnswerCorrect();
    } else {
      params.question1.questionStatus = QuestionStatus.answeredIncorrect;
      params.question2.questionStatus = QuestionStatus.answeredIncorrect;
      params.onAnswerWrong();
    }
    if (winGame()) {
      status = GameObjectStatus.answered;
      questionStatus = QuestionStatus.answeredCorrect;
    } else if (clickNumAvailable == 0) {
      status = GameObjectStatus.answered;
      questionStatus = QuestionStatus.answeredIncorrect;
      questions.forEach((element) {
        element.questionStatus = QuestionStatus.answeredCorrect;
      });
    }
  }

  bool winGame() {
    return questions.where((q) => q.questionStatus == QuestionStatus.answeredCorrect).length == questions.length;
  }

  @override
  reset() {
    super.reset();
    questions.forEach((element) {
      element.questionStatus = QuestionStatus.notAnswerYet;
    });
    clickNumAvailable = questions.length + 2;
  }
  
}