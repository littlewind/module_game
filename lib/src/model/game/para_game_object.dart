import 'package:game/src/model/game/game_object.dart';
import 'package:game/src/model/game/quiz_game_object.dart';
import 'package:models/module_models.dart';

class ParaGameObject extends GameObject {
  List<QuizGameObject> children = [];
  List<int> childrenIndex = [];

  ParaGameObject.fromQuestion(Question questionDb)
      : super.fromQuestion(questionDb);
}
